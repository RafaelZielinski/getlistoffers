package com.junioroffers.offer.domain;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.offer.OfferController;
import com.junioroffers.offer.domain.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.junioroffers.offer.domain.exceptions.OfferErrorResponse;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@WebMvcTest
@ContextConfiguration(classes = MockMvcConfig.class)
class OfferControllerTest implements SampleOfferDto{

    @Test
    void should_return_status_ok_when_get_for_offers(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        final List<OfferDto> expectedOffers = Arrays.asList(firstPythonGuyOffer(), secondJavaGuyOffer());
        String expectedResponseBody = objectMapper.writeValueAsString(expectedOffers);

        final MvcResult mvcResult = mockMvc.perform(get("/offers"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    void should_return_ok_when_get_for_by_id_offer(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        String expectedResponseBody = objectMapper.writeValueAsString(firstPythonGuyOffer());

        final MvcResult mvcResult = mockMvc.perform(get("/offers/1"))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    void should_return_status_not_found_get_offer(@Autowired MockMvc mockMvc, @Autowired ObjectMapper objectMapper) throws Exception {
        OfferErrorResponse offerErrorResponse = new OfferErrorResponse("Offer with id 232 not found", HttpStatus.NOT_FOUND);
        String expectedResponseBody = objectMapper.writeValueAsString(offerErrorResponse);

        final MvcResult mvcResult = mockMvc.perform(get("/offers/232"))
                .andExpect(status().isNotFound())
                .andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }
}

@Configuration(proxyBeanMethods = false)
class MockMvcConfig implements SampleOfferDto {
    @Bean
    OfferControllerErrorHandler offerControllerErrorHandler() {
        return new OfferControllerErrorHandler();
    }

    @Bean
    OfferService offerService() {
        OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
        return new OfferService(offerRepository) {
            @Override
            public List<OfferDto> getAll() {
                return Arrays.asList(firstPythonGuyOffer(), secondJavaGuyOffer());
            }
            @Override
            public OfferDto getById(String id) {
                if(id.equals("1")) {
                    return firstPythonGuyOffer();
                } else if(id.equals("2")) {
                    return secondJavaGuyOffer();
                } else {
                    throw new OfferNotFoundException(id);
                }
            }
        };
    }

    @Bean
    OfferController offerController(OfferService offerService) {
        return new OfferController(offerService);
    }
}