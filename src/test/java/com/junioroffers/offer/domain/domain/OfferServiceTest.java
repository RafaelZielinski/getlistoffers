package com.junioroffers.offer.domain.domain;

import com.junioroffers.offer.domain.OfferRepository;
import com.junioroffers.offer.domain.OfferService;
import com.junioroffers.offer.domain.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.BDDAssertions.then;

class OfferServiceTest implements SampleOfferDto, SampleOffer {

    private final OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
    private final OfferService offerService = new OfferService(offerRepository);

    @Test
    void should_return_all_offers() {
        when(offerRepository.findAll()).thenReturn(Arrays.asList(cybersourceOffer(), cdqPolandOffer()));
        assertThat(offerRepository.findAll()).isEqualTo(Arrays.asList(cybersourceOffer(), cdqPolandOffer()));

        OfferService offerService = new OfferService(offerRepository);
        final List<OfferDto> expectedOffers = Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());

        final List<OfferDto> allOffers = offerService.getAll();

        then(allOffers).containsExactlyInAnyOrderElementsOf(expectedOffers);

    }

    @Test
    void should_return_first_offer() {

        when(offerRepository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).thenReturn(Optional.of(cybersourceOffer()));
        assertThat(offerRepository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).isEqualTo(Optional.of(cybersourceOffer()));

        OfferService offerService = new OfferService(offerRepository);

        final OfferDto offerById = offerService.getById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074");

        then(offerById).isEqualTo(cybersourceOfferDto());


    }

    @Test
    void should_throw_offer_not_found_exception_when_no_offer_with_wrong_id() {

        when(offerRepository.findById("44")).thenReturn(Optional.empty());
        assertThat(offerRepository.findById("44")).isEqualTo(Optional.empty());


        Throwable thrown = catchThrowable(() -> offerService.getById("44"));

        assertThat(thrown).isInstanceOf(OfferNotFoundException.class)
                .hasMessage("Offer with id 44 not found");

    }
}
