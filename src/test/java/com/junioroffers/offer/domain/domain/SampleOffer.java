package com.junioroffers.offer.domain.domain;

import com.junioroffers.offer.domain.Offer;


public interface SampleOffer {

    default Offer cdqPolandOffer() {
        return new Offer("24ee32b6-6b15-11eb-9439-0242ac130002",
                "CDQ Poland",
                "Junior DevOps Engineer",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd"
        );
    }

    default Offer cybersourceOffer() {
        return new Offer("7b3e02b3-6b1a-4e75-bdad-cef5b279b074",
                "Cybersource",
                "Software Engineer - Mobile (m/f/d)",
                "4k - 8k PLN",
                "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn"
        );
    }

    default Offer sampleOfferWithoutId(String companyName, String position, String salary, String offerUrl) {
        final Offer offer = new Offer();
        offer.setCompanyName(companyName);
        offer.setOfferUrl(offerUrl);
        offer.setPosition(position);
        offer.setSalary(salary);
        return offer;
    }


}


