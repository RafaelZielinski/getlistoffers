package com.junioroffers.offer.domain.domain;

import com.junioroffers.JobOffersAplications;
import com.junioroffers.offer.domain.Offer;
import com.junioroffers.offer.domain.OfferRepository;
import com.junioroffers.offer.domain.OfferService;
import com.junioroffers.offer.domain.dto.OfferDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(classes = JobOffersAplications.class)
@ActiveProfiles("container")
@Testcontainers
class OfferServiceWithContainerTests implements SampleOffer {

    private static final String MONGO_VERSION = "4.4.4";
    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:" + MONGO_VERSION);

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_return_all_offers(@Autowired OfferRepository repository,
                                  @Autowired OfferService service) {
        Offer python = cybersourceOffer();
        Offer java = cdqPolandOffer();

        then(repository.findAll()).containsAll(Arrays.asList(python, java));
//        repository.saveAll(Arrays.asList(new Offer("23132", "fdaf", "dfaf", "fdasfdsaf", "dfasf")));
//        System.out.println(repository.findAll());
        final List<OfferDto> allOffers = service.getAll();
    }

    @Test
    void should_return_offers_by_id(@Autowired OfferRepository repository,
                                    @Autowired OfferService service) {
        Offer python = cybersourceOffer();

        then(repository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).contains(python);

    }
}
