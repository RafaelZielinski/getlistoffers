package com.junioroffers.offer.domain.domain.dto;

import com.junioroffers.offer.domain.Offer;
import com.junioroffers.offer.domain.OfferMapper;
import com.junioroffers.offer.domain.OfferMapperValues.*;
import com.junioroffers.offer.domain.domain.SampleOffer;
import com.junioroffers.offer.domain.dto.OfferDto;

public interface SampleOfferDto extends SampleOffer {

    default OfferDto firstPythonGuyOffer() {
        return  OfferMapper.mapToOfferDto(new Offer(("1")
        ,("Junior Java SE Developer for Automotive")
                , ("HARMAN Connected Services")
                , ("7k - 10k PLN")
                , ("https://nofluffjobs.com/pl/job/junior.com")));

    }
    default OfferDto secondJavaGuyOffer() {
        return OfferMapper.mapToOfferDto(new Offer(("2"),
                ("Java / Scala Developer II (Hybrid)")
                    , ("HERE Technologies")
                    , ("8k - 15k PLN")
                    , ("https://nofluffjobs.com/pl/job/java-scal.com")));
        }

    default OfferDto cdqPolandOfferDto()  {
        return OfferMapper.mapToOfferDto(cdqPolandOffer());
    }

    default OfferDto cybersourceOfferDto() {
        return OfferMapper.mapToOfferDto(cybersourceOffer());
    }
    }

