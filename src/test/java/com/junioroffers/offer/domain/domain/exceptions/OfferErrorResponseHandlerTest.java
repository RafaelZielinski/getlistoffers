package com.junioroffers.offer.domain.domain.exceptions;

import com.junioroffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.junioroffers.offer.domain.exceptions.OfferErrorResponse;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class OfferErrorResponseHandlerTest {

    @Test
    void should_return_correct_error() {
        //given
        OfferControllerErrorHandler offerControllerErrorHandler = new OfferControllerErrorHandler();
        final OfferNotFoundException givenException = new OfferNotFoundException("100");
        final OfferErrorResponse expectedResponse = new OfferErrorResponse("Offer with id 100 not found", HttpStatus.NOT_FOUND);
        //when
        final OfferErrorResponse actualResponse = offerControllerErrorHandler.offerNotFoun(givenException);
        //then
        assertThat(expectedResponse).isEqualTo(actualResponse);
    }
}
