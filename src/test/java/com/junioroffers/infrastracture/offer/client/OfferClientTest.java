package com.junioroffers.infrastracture.offer.client;
import com.junioroffers.infrastracture.offer.dto.OfferDto;
import com.junioroffers.infrastracture.offer.exceptions.RequestExecutionFailedException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

class OfferClientTest extends SampleJobOffer {

    final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
    RemoteOfferClient offerClient = new OfferClient(restTemplate);

    @Test
    void should_return_one_element_list_of_elements() {
        // GIVEN
        when(restTemplate.exchange(any()
                , any()
                , any()
                , (ParameterizedTypeReference<List<OfferDto>>) any()))
                .thenReturn(corectResponse(getOneOffer()));
        // WHEN
        List<OfferDto> offers = offerClient.getOffers("programming-masterpiece.com:5057/offers");
        // THEN
        assertThat(offers.size()).isEqualTo(1);
    }

    @Test
    void should_return_two_element_list_of_elements() {
        //GIVEN
        when(restTemplate.exchange(any()
                , any()
                , any()
                , (ParameterizedTypeReference<List<OfferDto>>) any() ))
                .thenReturn(corectResponse(getTwoOffers()));
        //WHEN
        List<OfferDto> offers = offerClient.getOffers("programming-masterpiece.com:5057/offers");
        //THEN
        assertThat(offers.size()).isEqualTo(2);
    }

    @Test
    void should_return_none_element_list_of_elements(){
        //GIVEN
        when(restTemplate.exchange(any()
                , any()
                , any()
                , (ParameterizedTypeReference<List<OfferDto>>) any() ))
                .thenReturn(corectResponse(getEmptyOffer()));
        //WHEN
        List<OfferDto> offers = offerClient.getOffers("programming-masterpiece.com:5057/offers");
        //THEN
        assertThat(offers.size()).isZero();
    }





}