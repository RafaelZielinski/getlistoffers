package com.junioroffers.infrastracture.offer.client;


import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.junioroffers.infrastracture.offer.dto.OfferDto;
import com.junioroffers.infrastracture.offer.exceptions.RequestExecutionFailedException;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.SocketUtils;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class OfferClientIntegrationTest {

    static OfferClient offerClient;
    WireMockServer wireMockServer;
    int port = SocketUtils.findAvailableTcpPort();
    @BeforeAll
    static void classSetup() {
        RestTemplate restTemplate = new RestTemplate();
        offerClient = new OfferClient(restTemplate);
    }

    @BeforeEach
    void testSetup() {
        wireMockServer = new WireMockServer(options().port(port));
        wireMockServer.start();
        WireMock.configureFor(port);
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void should_response_to_Offer_DTO_List() throws RequestExecutionFailedException {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(correctOfferResponse())));
        // when
        List<OfferDto> offers = offerClient.getOffers("http://localhost:" + port + "/offers");
        //then
        assertEquals(offers.size(), correctData().size());
        assertThat(offers).hasSameElementsAs(correctData());
    }

    @Test
    void should_Throw_Exception_When_The_Server_Does_Not_Respond() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(ok()));
        //when + then
        assertThrows(RequestExecutionFailedException.class,
                () -> offerClient.getOffers("http://localhost:" + port + "/wrong"));
    }

    @Test
    void should_Throw_Exception_When_The_Response_Is_Empty() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(aResponse()
                        .withFault(Fault.EMPTY_RESPONSE)));
        //when + then
        assertThrows(RequestExecutionFailedException.class,
                () -> offerClient.getOffers("http://localhost:" + port + "/offers"));
    }

    @Test
    void should_Throw_Exception_When_The_Connection_Is_Reset_By_Peer() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(aResponse()
                        .withFault(Fault.CONNECTION_RESET_BY_PEER)));
        //when + then
        assertThrows(RequestExecutionFailedException.class,
                () -> offerClient.getOffers("http://localhost:" + port + "/offers"));
    }


    private static List<OfferDto> correctData() {
        return Arrays.asList(
        new OfferDto("title", "company", "salary", "url"),
                new OfferDto("title2", "company", "salary", "url"));
    }

    private String correctOfferResponse() {
        return new Gson().toJson(
                correctData(),
                new TypeToken<List<OfferDto>>() {
                }.getType());
    }
}

