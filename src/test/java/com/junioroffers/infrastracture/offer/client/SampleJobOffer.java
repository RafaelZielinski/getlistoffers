package com.junioroffers.infrastracture.offer.client;

import com.junioroffers.infrastracture.offer.dto.OfferDto;
import org.springframework.http.ResponseEntity;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class SampleJobOffer {

    protected List<OfferDto> getEmptyOffer() {
        return Collections.emptyList();
    }

    protected List<OfferDto> getOneOffer() {
        return Arrays.asList(OfferDto.builder()
                    .jobPosition("title1")
                    .companyName("name")
                    .salary("5000")
                    .offerLink("www.sq.pl")
                    .build());
    }

    protected List<OfferDto> getTwoOffers() {
        return Arrays.asList(OfferDto.builder()
                        .jobPosition("title1")
                        .companyName("name")
                        .salary("3000")
                        .offerLink("www.sq.pl")
                        .build(),
                OfferDto.builder()
                        .jobPosition("title2")
                        .companyName("name")
                        .salary("4000")
                        .offerLink("www.sq.pl")
                        .build());
    }

    protected ResponseEntity<List<OfferDto>> corectResponse(List<OfferDto> offerDtoList) {
        return ResponseEntity.ok(offerDtoList);
    }
}
