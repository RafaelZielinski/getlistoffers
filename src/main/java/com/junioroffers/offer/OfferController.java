package com.junioroffers.offer;

import com.junioroffers.offer.domain.OfferService;
import com.junioroffers.offer.domain.dto.OfferDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/offers")
public class OfferController {

    private final OfferService offerService;

    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @GetMapping()
    public ResponseEntity<List<OfferDto>> getAll() {
       return ResponseEntity.ok(offerService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<OfferDto> getById(@PathVariable String id) {
        return ResponseEntity.ok(offerService.getById(id));
    }
}
