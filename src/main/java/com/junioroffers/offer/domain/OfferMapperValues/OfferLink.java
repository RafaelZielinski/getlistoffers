package com.junioroffers.offer.domain.OfferMapperValues;

import lombok.Value;

@Value
public class OfferLink {
    String link;
}
