package com.junioroffers.offer.domain.OfferMapperValues;

import lombok.Value;

@Value
public class Salary {
    String salary;
}
