package com.junioroffers.offer.domain.dto;

import lombok.*;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
@Builder
public class OfferDto {
    private final String id;
    private final String companyName;
    private final String jobPosition;
    private final String salary;
    private final String offerLink;
}
