package com.junioroffers.offer.domain.exceptions;

import lombok.Getter;

@Getter
public class OfferNotFoundException extends RuntimeException {

    private final String offerId;

    public OfferNotFoundException (String id) {
        super(String.format("Offer with id %s not found", id));
        this.offerId = id;
    }
}

