package com.junioroffers.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import java.time.Duration;

@Configuration
public class Config {


    @Bean
    public RestTemplate restTemplate(@Value("${offer.client.config.connectTimeout}") long connectTimeout,
                                    @Value("${offer.client.config.readTimeout}") long readTimeout) {
        return new RestTemplateBuilder()
                .setReadTimeout(Duration.ofMillis(readTimeout))
                .setConnectTimeout(Duration.ofMillis(connectTimeout))
                .build();
    }
}
