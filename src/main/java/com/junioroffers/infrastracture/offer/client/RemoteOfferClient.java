package com.junioroffers.infrastracture.offer.client;

import com.junioroffers.infrastracture.offer.dto.OfferDto;
import com.junioroffers.infrastracture.offer.exceptions.RequestExecutionFailedException;
import java.util.List;

public interface RemoteOfferClient {
    List<OfferDto> getOffers(String url) throws RequestExecutionFailedException;
}
