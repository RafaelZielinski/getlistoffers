package com.junioroffers.infrastracture.offer.client;

import com.junioroffers.infrastracture.offer.dto.OfferDto;

import com.junioroffers.infrastracture.offer.exceptions.RequestExecutionFailedException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class OfferClient implements RemoteOfferClient {

    private final RestTemplate restTemplate;

    @Override
    public List<OfferDto> getOffers(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(headers);
        try {
            ResponseEntity<List<OfferDto>> response = restTemplate
                    .exchange(URI.create(url),
                            HttpMethod.GET,
                            httpEntity,
                            new ParameterizedTypeReference<List<OfferDto>>() {
                    }
        );
            final List<OfferDto> body = response.getBody();
            return (body != null ? body : Collections.emptyList());
        } catch (Exception ex) {
            log.info(ex.getMessage());
            throw new RequestExecutionFailedException("Http request execution failed");
        }
    }
}
