package com.junioroffers.infrastracture.offer.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
public class OfferDto {
    private String companyName;
    private String jobPosition;
    private String salary;
    private String offerLink;
}
