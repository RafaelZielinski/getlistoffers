package com.junioroffers.infrastracture.offer.exceptions;

public class RequestExecutionFailedException extends RuntimeException {
    public RequestExecutionFailedException(String message) {
        super(message);
    }
}
